package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.repository.UserRepos;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;
import static org.mockito.Mockito.verify;

class UserServiceImpTest {


    @Mock
    UserRepos userRepos;

    @InjectMocks
    UserService service;

    @Test
    void newUserShouldMakeNewUserWithRepository() {
        User user = new User();
        user.setUsername("RAF");
        service.newUser(user);
        verify(userRepos).save(user);
    }

    @Test
    void getUserById() {
    }

    @Test
    void getAll() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void getUserByUsername() {
    }

    @Test
    void getCurrentUserObject() {
    }
}