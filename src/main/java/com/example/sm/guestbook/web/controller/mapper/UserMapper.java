package com.example.sm.guestbook.web.controller.mapper;

import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.model.UserDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
@Mapper(uses = UUIDMapper.class)
public interface UserMapper {
    User dtoToUser(UserDto userDto);

    UserDto userToDto(User newUser);

    List<UserDto> usersToDtos(List<User> users);
}
