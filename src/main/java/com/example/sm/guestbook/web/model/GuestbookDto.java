package com.example.sm.guestbook.web.model;

import com.example.sm.guestbook.model.Comment;
import java.time.LocalDateTime;
import java.util.Set;

public class GuestbookDto {

    private String id;

    private String post;

    private UserDto userDto;

    private String subject;

    private Set<Comment> comments;

    private LocalDateTime postTime;


    public LocalDateTime getPostTime() {
        return postTime;
    }

    public void setPostTime(LocalDateTime postTime) {
        this.postTime = postTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public UserDto getUser() {
        return userDto;
    }

    public void setUser(UserDto user) {
        this.userDto = user;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
