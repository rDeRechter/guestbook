package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.mapper.UserMapper;
import com.example.sm.guestbook.web.model.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserController(final UserService userService, final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> addNewUser(@RequestBody @Valid UserDto userDto){

        return new ResponseEntity<>(userMapper.userToDto(userService.newUser(userMapper.dtoToUser(userDto))), HttpStatus.CREATED);
    }

    @GetMapping("/")
    public List<UserDto> getAllUsers(){
        return userMapper.usersToDtos(userService.getAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable("id") UUID id){
        Optional<User> perhapsUser = userService.getUserById(id);
        return perhapsUser.map(user -> new ResponseEntity<>(userMapper.userToDto(user), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}/")
    public void deleteUserById(@PathVariable("id")UUID id){
        userService.deleteById(id);
    }
}