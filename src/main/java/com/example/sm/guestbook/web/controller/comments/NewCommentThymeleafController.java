package com.example.sm.guestbook.web.controller.comments;

import com.example.sm.guestbook.Service.CommentService;
import com.example.sm.guestbook.Service.GuestbookService;
import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.Comment;
import com.example.sm.guestbook.model.Guestbook;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.GuestbookControllerDelegate;
import com.example.sm.guestbook.web.controller.mapper.CommentMapper;
import com.example.sm.guestbook.web.model.CommentDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/guestbook/{guestbookId}/comment/new")
public class NewCommentThymeleafController {

    private final UserService userService;
    private final GuestbookService guestbookService;
    private final GuestbookControllerDelegate delegate;
    private final CommentMapper commentMapper;
    private final CommentService commentService;

    public NewCommentThymeleafController(UserService userService, GuestbookService guestbookService, GuestbookControllerDelegate delegate, CommentMapper commentMapper, CommentService commentService) {
        this.userService = userService;
        this.guestbookService = guestbookService;
        this.delegate = delegate;
        this.commentMapper = commentMapper;
        this.commentService = commentService;
    }
//    @Secured("{ROLE_USER, ROLE_ADMIN}")
    @GetMapping("/")
    public String forward(Model model, @PathVariable("guestbookId") String guestbookId) {
        model.addAttribute("guestbook",delegate.getAGuestbook(UUID.fromString(guestbookId)));

        return "comments/createNewComment.html";
    }

    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }

//    @Secured("{ROLE_USER, ROLE_ADMIN}")
    @PostMapping(value="/", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.TEXT_HTML_VALUE})
    public String createComment(@Valid CommentDto commentDto, @PathVariable("guestbookId") String guestbookId, HttpServletRequest request){
        Principal principal = request.getUserPrincipal();
        User activeUser = userService.getUserByUsername(principal.getName());
        Optional<Guestbook> perhapsGuestbook = guestbookService.getGuestbookById(UUID.fromString(guestbookId));
        if (perhapsGuestbook.isPresent()){
            Comment comment = commentMapper.dtoToComment(commentDto);
            comment.setGuestbook(perhapsGuestbook.get());
            comment.setUser(activeUser);
            commentService.addComment(comment);
        }return "redirect:/guestbook/" + guestbookId + "/comment/";
    }
}
