package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/guestbook")
public class GuestbookThymeleafController {
    private final GuestbookControllerDelegate delegate;
    private final UserService userService;
    public GuestbookThymeleafController(GuestbookControllerDelegate delegate, UserService userService) {
        this.delegate = delegate;
        this.userService = userService;
    }



    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }
    @GetMapping(value = "/", produces = {MediaType.TEXT_HTML_VALUE})
    public String showAllGuestbooks(Model model){
        model.addAttribute("guestbooks", delegate.getAllGuestbooks());
        return "guestbooks/guestbookList.html";
    }





}
