package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.model.GuestbookDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
@Controller
@RequestMapping("/guestbook/new")
public class NewGuestbookController {

    private final GuestbookControllerDelegate delegate;
    private final UserService userService;
    public NewGuestbookController(GuestbookControllerDelegate delegate, UserService userService) {
        this.delegate = delegate;
        this.userService = userService;
    }

    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }
    @GetMapping("/")
    public String forward() {
        return "guestbooks/createNewGuestbook.html";
    }

    @PostMapping(value="/", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.TEXT_HTML_VALUE})
    public String createNewTopic(@Validated GuestbookDto dto, HttpServletRequest request){
        Principal principal = request.getUserPrincipal();
        User activeUser = userService.getUserByUsername(principal.getName());
        delegate.createNewPost(dto, activeUser.getId());
        return "redirect:/guestbook/";
    }
}
