package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.web.model.UserDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final UserControllerDelegate delegate;

    public RegisterController(UserControllerDelegate delegate) {
        this.delegate = delegate;
    }

    @GetMapping("/")
    public String forward() {
        return "users/register.html";
    }

    @PostMapping(value="/", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = {MediaType.TEXT_HTML_VALUE})
    public String register(@Validated UserDto userDto){
        delegate.addNewUser(userDto);
        return "redirect:/login/";
    }
}