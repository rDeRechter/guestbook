package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.GuestbookService;
import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.Guestbook;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.mapper.GuestbookMapper;
import com.example.sm.guestbook.web.model.GuestbookDto;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class GuestbookControllerDelegate {

    private final GuestbookMapper guestbookMapper;
    private final GuestbookService guestbookService;
    private final UserService userService;

    public GuestbookControllerDelegate(GuestbookMapper guestbookMapper, GuestbookService guestbookService, UserService userService) {
        this.guestbookMapper = guestbookMapper;
        this.guestbookService = guestbookService;
        this.userService = userService;
    }

    public List<GuestbookDto> getAllGuestbooks(){
        return guestbookMapper.guestbooksToDtos(guestbookService.getAll());
    }

    public GuestbookDto getAGuestbook(UUID id){
        Optional<Guestbook> perhapsGuestbook = guestbookService.getGuestbookById(id);
        return perhapsGuestbook.map(guestbookMapper::guestbookToDto).orElse(null);
    }

    public GuestbookDto createNewPost(GuestbookDto guestbookDto, UUID userId){
        Guestbook guestbook = guestbookMapper.dtoToGuestbook(guestbookDto);
        Optional<User> perhapsUser = userService.getUserById(userId);
        if (perhapsUser.isPresent()){
            guestbook.setUser(perhapsUser.get());
            return guestbookMapper.guestbookToDto(guestbookService.save(guestbook));
        }return null;
    }
}
