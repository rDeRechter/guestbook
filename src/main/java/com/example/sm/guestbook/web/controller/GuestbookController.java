package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.GuestbookService;
import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.Guestbook;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.mapper.GuestbookMapper;
import com.example.sm.guestbook.web.model.GuestbookDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/guestbook")
public class GuestbookController {

    private final GuestbookMapper guestbookMapper;
    private final GuestbookService guestbookService;
    private final UserService userService;

    public GuestbookController(GuestbookMapper guestbookMapper, GuestbookService guestbookService, UserService userService) {
        this.guestbookMapper = guestbookMapper;
        this.guestbookService = guestbookService;
        this.userService = userService;
    }

    @GetMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GuestbookDto> getAllGuestbooks(){
        return guestbookMapper.guestbooksToDtos(guestbookService.getAll());
    }

    @PostMapping(value = "/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuestbookDto> createNewPost(@RequestBody @Valid GuestbookDto guestbookDto, @PathVariable("userId") UUID userId){
        Guestbook guestbook = guestbookMapper.dtoToGuestbook(guestbookDto);
        Optional<User> perhapsUser = userService.getUserById(userId);
        if (perhapsUser.isPresent()){
            guestbook.setUser(perhapsUser.get());
            return new ResponseEntity<>(guestbookMapper.guestbookToDto(guestbookService.save(guestbook)),HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




}
