package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/wrongUserError")
@Controller
public class WrongUserErrorThymeleafController {

    private final UserService userService;

    public WrongUserErrorThymeleafController(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }
    @GetMapping("")
    public String showErrorPage(){
        return "/users/wrongUserError.html";
    }
}
