package com.example.sm.guestbook.web.model;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class CommentDto {

    private String id;

    @NotBlank
    private String comment;

    private UserDto user;

    private GuestbookDto guestbook;

    private LocalDateTime commentTime;

    public LocalDateTime getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(LocalDateTime commentTime) {
        this.commentTime = commentTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public GuestbookDto getGuestbook() {
        return guestbook;
    }

    public void setGuestbook(GuestbookDto guestbook) {
        this.guestbook = guestbook;
    }
}
