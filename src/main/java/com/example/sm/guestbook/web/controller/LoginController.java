package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }
    @GetMapping("")
    public String forward(){
        return "login";
    }

    @PostMapping("")
    public String login(){
        return "redirect:/user/";
    }
}
