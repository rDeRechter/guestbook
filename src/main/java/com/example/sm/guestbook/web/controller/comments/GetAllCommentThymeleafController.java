package com.example.sm.guestbook.web.controller.comments;

import com.example.sm.guestbook.Service.CommentService;
import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.Comment;
import com.example.sm.guestbook.web.controller.GuestbookControllerDelegate;
import com.example.sm.guestbook.web.controller.mapper.CommentMapper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/guestbook/{guestbookId}/comment")
public class GetAllCommentThymeleafController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final GuestbookControllerDelegate delegate;
    private final UserService userService;

    public GetAllCommentThymeleafController(CommentService commentService, CommentMapper commentMapper, GuestbookControllerDelegate delegate, UserService userService) {
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.delegate = delegate;
        this.userService = userService;
    }

    @GetMapping(value = "/", produces = {MediaType.TEXT_HTML_VALUE})
    public String showAllComments(Model model, @PathVariable("guestbookId") String guestbookId){
        List<Comment> commentCheckList = commentService.getAllCommentsFromGuestbook( guestbookId);
        commentCheckList.forEach(comment -> System.out.println(comment.getComment()));
        model.addAttribute("guestbook",delegate.getAGuestbook(UUID.fromString(guestbookId)));
        model.addAttribute("comments", commentMapper.commentsToDtos(commentService.getAllCommentsFromGuestbook( guestbookId)));
        return "comments/commentList.html";
    }
    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }

    @DeleteMapping(value = "/{commentId}/del/", produces = {MediaType.TEXT_HTML_VALUE},  consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String goToNewComments( @PathVariable("commentId") String commentId, @PathVariable("guestbookId") String guestbookId){
        commentService.deleteComment(UUID.fromString(commentId));
        System.out.println(commentId);
        return "redirect:/guestbook/" +guestbookId + "/comment/";
    }
}
