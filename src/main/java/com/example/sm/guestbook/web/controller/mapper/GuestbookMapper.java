package com.example.sm.guestbook.web.controller.mapper;

import com.example.sm.guestbook.model.Guestbook;
import com.example.sm.guestbook.web.model.GuestbookDto;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(uses = {UUIDMapper.class, UserMapper.class})
public interface GuestbookMapper {

    Guestbook dtoToGuestbook(GuestbookDto guestbookDto);

    GuestbookDto guestbookToDto(Guestbook guestbook);

    List<GuestbookDto> guestbooksToDtos(List<Guestbook> guestbookDtos);
}
