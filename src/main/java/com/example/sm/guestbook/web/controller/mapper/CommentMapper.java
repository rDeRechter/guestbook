package com.example.sm.guestbook.web.controller.mapper;

import com.example.sm.guestbook.model.Comment;
import com.example.sm.guestbook.web.model.CommentDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
@Mapper(uses = {UUIDMapper.class, UserMapper.class, GuestbookMapper.class})
public interface CommentMapper {
    Comment dtoToComment(CommentDto commentDto);
    CommentDto commentToDto(Comment comment);
    List<CommentDto> commentsToDtos(List<Comment> allCommentsFromGuestbook);
}
