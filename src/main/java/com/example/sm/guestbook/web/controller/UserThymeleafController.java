package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserThymeleafController {

    private final UserControllerDelegate userControllerDelegate;
    private final UserService userService;
    public UserThymeleafController(UserControllerDelegate userControllerDelegate, UserService userService) {
        this.userControllerDelegate = userControllerDelegate;
        this.userService = userService;
    }

    @ModelAttribute("role")
    public String getRole() {
        return userService.getCurrentUserObject()==null?null:userService.getCurrentUserObject().getRole();
    }

    @GetMapping(value = "/", produces = {MediaType.TEXT_HTML_VALUE})
    public String showAllUsers(Model model){
        model.addAttribute("users", userControllerDelegate.getAllUsers());
        return "users/list.html";
    }
}
