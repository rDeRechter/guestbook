package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.mapper.UserMapper;
import com.example.sm.guestbook.web.model.UserDto;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserControllerDelegate {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserControllerDelegate(final UserService userService, final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public UserDto addNewUser(UserDto userDto){

        return userMapper.userToDto(userService.newUser(userMapper.dtoToUser(userDto)));
    }
    public List<UserDto> getAllUsers(){
        System.out.println(userService.getUserByUsername("admin").getId());
        return userMapper.usersToDtos(userService.getAll());
    }

    public UserDto getUserById(UUID id){
        Optional<User> perhapsUser = userService.getUserById(id);
        return perhapsUser.map(userMapper::userToDto).orElse(null);
    }

    public void deleteUserById(UUID id){
        userService.deleteById(id);
    }

}
