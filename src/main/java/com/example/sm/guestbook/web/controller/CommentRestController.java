package com.example.sm.guestbook.web.controller;

import com.example.sm.guestbook.Service.CommentService;
import com.example.sm.guestbook.Service.GuestbookService;
import com.example.sm.guestbook.Service.UserService;
import com.example.sm.guestbook.model.Comment;
import com.example.sm.guestbook.model.Guestbook;
import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.mapper.CommentMapper;
import com.example.sm.guestbook.web.model.CommentDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/guestbook/{guestbookId}/comment")
public class CommentRestController {

    private final CommentMapper commentMapper;
    private final CommentService commentService;
    private final UserService userService;
    private final GuestbookService guestbookService;

    public CommentRestController(CommentMapper commentMapper, CommentService commentService, UserService userService, GuestbookService guestbookService) {
        this.commentMapper = commentMapper;
        this.commentService = commentService;
        this.userService = userService;
        this.guestbookService = guestbookService;
    }

    @PostMapping("/{userId}")
    public ResponseEntity<Comment> createComment(@RequestBody @Valid CommentDto commentDto, @PathVariable("guestbookId")UUID guestbookId,@PathVariable("userId")UUID userId){
        Optional<User> perhapsuser = userService.getUserById(userId);
        Optional<Guestbook> perhapsGuestbook = guestbookService.getGuestbookById(guestbookId);
        if (perhapsGuestbook.isPresent() && perhapsuser.isPresent()){
            Comment comment = commentMapper.dtoToComment(commentDto);
            comment.setGuestbook(perhapsGuestbook.get());
            comment.setUser(perhapsuser.get());
            return new ResponseEntity<>(commentService.addComment(comment), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
