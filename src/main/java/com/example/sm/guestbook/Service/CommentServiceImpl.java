package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.Comment;
import com.example.sm.guestbook.repository.CommentRepos;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class CommentServiceImpl implements CommentService{

    private final CommentRepos repos;

    public CommentServiceImpl(CommentRepos repos) {
        this.repos = repos;
    }

    @Override
    public Comment addComment(Comment comment) {
        comment.setCommentTime(LocalDateTime.now());
        return repos.save(comment);
    }

    @Override
    public List<Comment> getAllCommentsFromGuestbook(String guestbookId) {
        return repos.getAllByGuestbookIdOrderByCommentTimeDesc(UUID.fromString(guestbookId));
    }

    @Override
    public void deleteComment(UUID id) {
        repos.deleteById(id);
    }
}
