package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.Guestbook;
import com.example.sm.guestbook.repository.GuestbookRepos;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class GuestbookServiceImpl implements GuestbookService {

    private final GuestbookRepos guestbookRepos;

    public GuestbookServiceImpl(GuestbookRepos guestbookRepos) {
        this.guestbookRepos = guestbookRepos;
    }

    @Override
    public List<Guestbook> getAll() {
        return guestbookRepos.findAll();
    }

    @Override
    public Guestbook save(Guestbook guestbook) {
        guestbook.setPostTime(LocalDateTime.now());
        return guestbookRepos.save(guestbook);
    }

    @Override
    public Optional<Guestbook> getGuestbookById(UUID guestbookId) {
        return guestbookRepos.findById(guestbookId);
    }

}
