package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.Comment;
import java.util.List;
import java.util.UUID;

public interface CommentService {

    Comment addComment(Comment comment);

    List<Comment> getAllCommentsFromGuestbook(String guestbookId);

    void deleteComment(UUID id);
}
