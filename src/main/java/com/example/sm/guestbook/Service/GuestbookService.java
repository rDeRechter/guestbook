package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.Guestbook;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GuestbookService {

    List<Guestbook> getAll();

    Guestbook save(Guestbook guestbook);

    Optional<Guestbook> getGuestbookById(UUID guestbookId);
}
