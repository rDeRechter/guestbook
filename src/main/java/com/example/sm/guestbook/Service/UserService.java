package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.web.controller.comments.GetAllCommentThymeleafController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

    User newUser(User user);

    Optional<User> getUserById(UUID userId);

    List<User> getAll();

    void deleteById(UUID id);

    User getUserByUsername(String s);

    User getCurrentUserObject();
}
