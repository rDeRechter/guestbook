package com.example.sm.guestbook.Service;

import com.example.sm.guestbook.model.User;
import com.example.sm.guestbook.repository.UserRepos;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImp implements UserService{

    private final UserRepos repos;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImp(UserRepos repos, PasswordEncoder passwordEncoder) {
        this.repos = repos;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User newUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole("ROLE_USER");
        return repos.save(user);
    }

    @Override
    public Optional<User> getUserById(UUID userId) {
        return repos.findById(userId);
    }

    @Override
    public List<User> getAll() {
        return repos.findAll();
    }

    @Override
    public void deleteById(UUID id) {
        repos.deleteById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return repos.getByUsername(username);
    }
    private String getCurrentUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication==null?null:authentication.getName();
    }

    private User getCurrentUserObjectByName(String username) {
        return repos.findByUsername(username);
    }

    public User getCurrentUserObject() {
        String username = this.getCurrentUserName();
        return username==null?null:this.getCurrentUserObjectByName(username);
    }
}
