package com.example.sm.guestbook.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.sql.DataSource;


@Configuration
@EnableWebSecurity
@ComponentScan
@Order(1000)
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebSecurityConfigurer<WebSecurity> securityConfigurer(DataSource ds) {
        return new WebSecurityConfigurerAdapter() {
            @Override
            protected void configure(AuthenticationManagerBuilder auth) throws Exception {
                auth
                        .jdbcAuthentication()
                        .passwordEncoder(new BCryptPasswordEncoder())
                        .dataSource(ds)
                        .usersByUsernameQuery("select username, password, enabled from user where username = ?")
                        .authoritiesByUsernameQuery("select username, enabled from user where username = ?")
                ;
            }

            @Override
            protected void configure(HttpSecurity http) throws Exception {

                http.csrf().disable().authorizeRequests().antMatchers("/**")
                        .authenticated()
                        .and().authorizeRequests().antMatchers("/mdbootstrap/**").permitAll().anyRequest().permitAll().and()
                        .formLogin()
                        .loginPage("/login")
                        .loginProcessingUrl("/authenticateUser")
                        .permitAll()
                        .and()
                        .logout()
                        .permitAll()
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                        .and()
                        .exceptionHandling()
                        .accessDeniedPage("/login");
            }

            @Override
            public void configure(WebSecurity webSecurity) throws Exception {
                webSecurity
                        .ignoring()
                        .antMatchers("/register/", "/", "/commentList/", "/mdbootstrap/**","/images/**", "/");
            }
        };


    }

}
