package com.example.sm.guestbook.repository;

import com.example.sm.guestbook.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;

public interface UserRepos extends JpaRepository<User, UUID> {
    User getByUsername(String username);

    User findByUsername(String username);
}
