package com.example.sm.guestbook.repository;

import com.example.sm.guestbook.model.Comment;
import com.example.sm.guestbook.model.Guestbook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface GuestbookRepos extends JpaRepository<Guestbook, UUID> {
    Guestbook getByComments(Comment comment);
}
