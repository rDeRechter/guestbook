package com.example.sm.guestbook.repository;

import com.example.sm.guestbook.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.UUID;

public interface CommentRepos extends JpaRepository<Comment, UUID> {
    List<Comment> getAllByGuestbookIdOrderByCommentTimeDesc(UUID guestbookId);
}
