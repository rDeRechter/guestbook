package com.example.sm.guestbook.model;

import com.example.sm.guestbook.repository.UserRepos;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
public class User extends IdModel{


    @NotBlank
    private String firstName, lastName, username;

    @NotBlank
    private String email;

    @NotBlank
    private String password, role;

    private int enabled = 1;

    @OneToMany(mappedBy = "user")
    private Set<Guestbook> guestbooks;

    @OneToMany(mappedBy = "user")
    private Set<Comment> comments;

    public User() {
    }

    public Set<Guestbook> getGuestbooks() {
        return guestbooks;
    }

    public void setGuestbooks(Set<Guestbook> guestbooks) {
        this.guestbooks = guestbooks;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }


}
