package com.example.sm.guestbook.model;

import javax.persistence.*;
import java.util.UUID;

@MappedSuperclass
public abstract class IdModel {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }
}
